using Uno;
using Uno.Collections;
using Uno.Compiler.ExportTargetInterop;

using Fuse;
using Fuse.Scripting;
using Fuse.Reactive;

public class Logea: NativeModule
{
    public Logea()
    {
        AddMember(new NativeFunction("Log", (NativeCallback)Log));
    }

    static object Log(Context c, object[] args)
    {
        foreach (var arg in args)
            debug_log arg;

        return null;
    }
}
